#!/usr/bin/env bash
set -x
set -euo pipefail
    
WORKSPACE="$(pwd)"
SUBNET="172.18.0.0/16"
APP_IP_ADDRESS="172.18.0.4"
TCP_PORT="8888"

if [[ "${#}" != "1" ]]; then
    echo "ERROR: Please provide a version which you want to deploy."
    echo "Example: ${0} 0.1.2.3"
    exit 3
fi

APP_VERSION=${1}

docker build -t test-app-v"${APP_VERSION}" ${WORKSPACE}/docker/test-app-v"${APP_VERSION}"/
docker run -d --net test-app --ip ${APP_IP_ADDRESS} -p ${TCP_PORT}:${TCP_PORT} test-app-v"${APP_VERSION}" \
&& status_code=$(curl --silent --output /dev/null --write-out "%{http_code}" http://localhost:${TCP_PORT})

if [[ "${status_code}" != "200" ]]; then
    echo "ERROR: An instance of the ${APP_VERSION} has not been run! Please check Docker logs for the details."
    exit 5
fi

# Changing configuration of the proxy, now it should point to an instance with the new version
sed -i -r "s/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}:[0-9]{2,5}/${APP_IP_ADDRESS}:${TCP_PORT}/" ${WORKSPACE}/docker/proxy/conf.d/*

docker kill -s HUP proxy

# Some time is needed to apply the new config
sleep 5

status_code=$(curl --silent --output /dev/null --write-out "%{http_code}" http://localhost:80) 

if [[ "${status_code}" != "200" || ! $(curl -s http://localhost:80 | grep ${APP_VERSION}) ]]; then
    echo "ERROR: The deployment of a new version has failed!"
    exit 4
fi

echo "INFO: The version ${APP_VERSION} has been successfully deployed."
