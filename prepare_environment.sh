#!/usr/bin/env bash
set -x
set -euo pipefail

WORKSPACE="$(pwd)"
SUBNET="172.18.0.0/16"
APP_IP_ADDRESS="172.18.0.2"

docker pull nginx:1.15.5
docker network create test-app --subnet=${SUBNET}

docker build -t test-app-v"${APP_VERSION}" ${WORKSPACE}/docker/test-app-v"${APP_VERSION}"/
docker build -t proxy ${WORKSPACE}/docker/proxy/

#docker run -d --net test-app -p 127.0.0.1:8080:8080 --ip ${APP_IP_ADDRESS} test-app-v"${APP_VERSION}" && \
docker run -d --net test-app --ip ${APP_IP_ADDRESS} test-app-v"${APP_VERSION}" && \
docker run --name proxy -d --net test-app -p 80:80 -v ${WORKSPACE}/docker/proxy/conf.d:/etc/nginx/conf.d proxy && \
status_code=$(curl --silent --output /dev/null --write-out "%{http_code}" http://localhost:80)

# A simple test that the app is responding once the env is up 
if [[ "${status_code}" != "200" ]]; then
    echo "There was some issue during the deployment. Please check the containers output."
    exit 2
fi

curl --silent http://localhost:80
echo "The application is up an running."
