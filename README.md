**VGS devops test task 2.0**

This repository contains a "solution" of the test task for the DevOps position applicants. The solution is a basic implementation of the "blue/green deployment" strategy that could be applied for cases when we need to update an in-complex stateless application. Docker has been chosen as a deployment target since it is, in fact, the most commonly used platform for new web applications nowadays; also,it reduced the amount of effort, which was needed for the implementation, allowing to concentrate on the task showcase.

---

## Prerequsites

You need to have **Git** VCS (https://git-scm.com/downloads) and **Docker** (https://www.docker.com/get-started) installed in your system. Bash language interpreter should already be present in most modern environments. If it is not the case, then Bash, which comes with Git, can be used. Some CLI tools, e.g., __curl__, are also needed but they usually included into any Bash environment, or must be installed separately. 
To make sure that your Docker instance is installed correctly, run from the CLI:
```
docker --version
```
The output should be similar to the one below:
```
Docker version 18.03.1-ce, build 9ee9f40
```

## How to run

1. Clone this repo. From the CLI it can be done like this:
```
git clone https://vault60@bitbucket.org/vault60/vgs-test-task.git
```
2. Go inside the cloned repo directory:
```
cd vgs-test-task
```
3. Run a script that spins up the initial version of the test app and the proxy server:
```
APP_VERSION=0.0.1 bash prepare_environment.sh
```
Please note that the app version **must** be passed to the script as an environment variable. Since only two versions of the app are available in the scope of this task, we are specifying the version "0.0.1". As a result, you should see the the source code of the test app web-page on the console. You can also then open it using a web-browser by this link - http://localhost:80.
4. Now we have a version of the app which we can update, so we can proceed with our next deployment. To trigger the process, run the other Bash script:
```
bash deploy_new_version.sh 0.0.2
```
This time the version is provided to the script as a positional parameter. Once the proccess is finished, the script will print an "INFO" message to the console output, and the new version of the app will be available by the same http address. Also, we can kill the container with the previous version of the app after making sure that the new version works correct.

## Limitations

The automation scrips do not handle a situation when some infrastructure elements already exist and rely on the underlying tools, like Docker, or on user in this. It is assumed that the infrastructure is built from scratch. If you need to re-run the deployments, please check if you have the Docker network and the corresponding images and containers present in your environment. The next commands can be used for a quick clean-up:
```
docker ps --all | grep -E "(test-app|proxy)" | cut -f1 -d' ' | xargs docker kill
docker ps --all | grep -E "(test-app|proxy)" | cut -f1 -d' ' | xargs docker rm
docker network rm test-app
```
Then delete the repo and clone it again.
Please make sure that name templates from these commands do not interfere with resources in your Docker environment, or you can loose them! Please adjust the commands if needed.

## Potential improvements and 'nice-to-have's

This solutions was created with assumption that we have to deal with a case when there are not any additional features that are present in real-world products: persistent datastore with changing data schemas, application level states during connections, dependencies on some other services. But even for such simple application the next improvements could (and should) be used:
* generating configuration files from templates;
* defining and running the containers as a whole set using specially designed tools (e.g., Docker Compose) instead of the Bash script;
* receiving all potentially mutable values (IPs, hostnames etc.) as a parameters, not having them mixed with a code.
